package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "支店定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_RENBAN = "売上ファイル名が連番になっていません";
	private static final String FILE_NO_FORMAT = "のフォーマットが不正です";
	private static final String TOTAL_EXCEED = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		//●(ｴﾗｰ処理3-1)ｺﾏﾝﾄﾞﾗｲﾝ引数が渡されているか
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		//●2-1処理
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			//●(ｴﾗｰ処理3-2)ﾌｧｲﾙかどうかも一緒に確かめる
			if (files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//●(ｴﾗｰ処理2-1)連番かどうか
		Collections.sort(rcdFiles);

		//●(ｴﾗｰ処理2-1)売上ﾌｧｲﾙ連番かどうか
		for (int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_RENBAN);
				return;
			}
		}

		//●(2-2処理)
		BufferedReader br = null;

		for (int i = 0; i < rcdFiles.size(); i++) {

			try {
				//●rcdFilesのn個目の要素を取り出す
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				List<String> fileContent = new ArrayList<>();

				String line;
				while ((line = br.readLine()) != null) {
					fileContent.add(line);
				}

				//●(ｴﾗｰ処理2-4)売上ﾌｧｲﾙの中身が2桁か
				if (fileContent.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + FILE_NO_FORMAT);
					return;
				}

				//●(ｴﾗｰ処理2-3)売上ﾌｧｲﾙの支店ｺｰﾄﾞが支店定義ﾌｧｲﾙに存在するか確認
				if (!branchSales.containsKey(fileContent.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_NO_FORMAT);
					return;
				}

				//●1-2で作ったMapへ上書きするイメージ
				//●String型からlong型にする
				long fileSale = Long.parseLong(fileContent.get(1));
				//●(ｴﾗｰ処理3-3)売上ﾌｧｲﾙの売上金額が数字かどうか
				if (!fileContent.get(1).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//●Mapの支店ｺｰﾄﾞとlistの支店ｺｰﾄﾞを紐づけた売上金額を加算する、
				//●Mapにputする。(支店ｺｰﾄﾞと売上金額)
				Long saleAmount = branchSales.get(fileContent.get(0)) + fileSale;

				//(ｴﾗｰ処理2-2)売上金額の合計が10桁超えた場合
				if (saleAmount >= 10000000000L) {
					System.out.println(TOTAL_EXCEED);
					return;
				}
				branchSales.put(fileContent.get(0), saleAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//●(ｴﾗｰ処理1-1)支店定義ﾌｧｲﾙが存在しない場合
			if (!file.exists()) {
				System.out.println(FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// (処理内容1-2)
				//支店コードと支店名を別々に分割をするため
				String[] items = line.split(",");

				//●(ｴﾗｰ処理1-2)支店定義ﾌｧｲﾙﾌｫｰﾏｯﾄを確認
				if ((items.length != 2) || (!items[0].matches("^[0-9]{3}$"))) {
					System.out.println(FILE_INVALID_FORMAT);
					return false;
				}

				//1つ目は支店コードと支店名、2つ目は支店コードと売上金額(0にする)
				branchNames.put(items[0], items[1]);
				branchSales.put(items[0], 0L);

				System.out.println(line);
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {
		//●(処理内容3-1)
		//●ﾌｧｲﾙを作成する
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//●ﾌｧｲﾙに書き込む支店ｺｰﾄﾞと支店名と売上金額を書き込む
			for (String key : branchSales.keySet()) {
				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}